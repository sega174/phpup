<?php

return [
    'user' => [
        'class' => 'frontend\modules\user\Module',
    ],
    'post' => [
        'class' => 'frontend\modules\post\Module',
    ],
];
