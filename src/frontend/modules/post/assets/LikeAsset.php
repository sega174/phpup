<?php

namespace frontend\modules\post\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Ассет для просмотра списка установленного оборудования
 */
class LikeAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/post/assets/view/js';
    public $depends = [JqueryAsset::class];
    public $js = ['like.js'];
}
